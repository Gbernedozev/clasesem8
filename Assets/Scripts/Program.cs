﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GustavoBernedo
{
   
    class AudioyVideo
    {
        private string _name;
        public string name
        {
            get { return _name; }
        }

        protected AudioyVideo(string name)
        {
            _name = name;
        }

        public virtual void ShowMenu()
        {
            Console.WriteLine("Ha elegido :");
        }

    }


    class EquipSonido:AudioyVideo
    {
        
        public EquipSonido(string name) : base(name)
        {
            Console.WriteLine("Eligiendo.....");
        }

        public void CastSpell()
        {
        }

        public override void ShowMenu()
        {
            string action;

            base.ShowMenu();

            Console.WriteLine("(1) - Reproductor MP3");
            Console.WriteLine("(2) - Reproductor MP4");

            action = Console.ReadLine();
            Console.WriteLine("Action selected " + action);
        }
    }
    class AccesoriosVideo:AudioyVideo
    {
        public AccesoriosVideo(string name) : base(name)
        {
            Console.WriteLine("Eligiendo.....");
        }

        public void CastSpell()
        {
        }

        public override void ShowMenu()
        {
            string action;

            base.ShowMenu();

            Console.WriteLine("(1) - Cable HDMI");
            Console.WriteLine("(2) - Cables V analogico");

            action = Console.ReadLine();
            Console.WriteLine("Action selected " + action);
        }
    }
    class Audifonos:AudioyVideo
    {
        public Audifonos(string name) : base(name)
        {
            Console.WriteLine("Eligiendo.....");
        }

        public void CastSpell()
        {
        }

        public override void ShowMenu()
        {
            string action;

            base.ShowMenu();

            Console.WriteLine("(1) - audifonos on ear");
            Console.WriteLine("(2) - audifonos bluetooth");
            Console.WriteLine("(3) - audifonos in ear");

            action = Console.ReadLine();
            Console.WriteLine("Action selected " + action);
        }
    }
    class ReprodVideo:AudioyVideo
    {
        public ReprodVideo(string name) : base(name)
        {
            Console.WriteLine("Eligiendo.....");
        }

        public void CastSpell()
        {
        }

        public override void ShowMenu()
        {
            string action;

            base.ShowMenu();

            Console.WriteLine("(1) - dvd portatil");
            Console.WriteLine("(2) - celular");
            Console.WriteLine("(3) - monitor");

            action = Console.ReadLine();
            Console.WriteLine("Action selected " + action);
        }
    }
    class AccesoriosSonido:AudioyVideo
    {
        public AccesoriosSonido(string name) : base(name)
        {
            Console.WriteLine("Eligiendo.....");
        }

        public void CastSpell()
        {
        }

        public override void ShowMenu()
        {
            string action;

            base.ShowMenu();

            Console.WriteLine("(1) - Accesorio1");
            Console.WriteLine("(2) - Accesorio2");
            Console.WriteLine("(3) - Accesorio3");

            action = Console.ReadLine();
            Console.WriteLine("Action selected " + action);
        }
    }
}
